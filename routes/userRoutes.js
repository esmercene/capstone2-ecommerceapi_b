const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')


// User Registration Routes
router.post('/register', (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

// Get all user
router.get('/', (request, response) => {
	UserController.getAllUsers().then((result) => {
		response.send(result)
	})
})



// Retrieve user details
router.get('/:id/details',  (request, response) =>  {
	UserController.getUserDetails(request.params.id).then((result) => { response.send(result)

	})
})

// Login User / User Authentication
router.post('/login', (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

// Update a user
router.patch('/:userId/update', auth.verify, (request, response) => {
	const data = {
		user: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.updateUser(request.params.userId, data).then((result) => {
		response.send(result)
	})
})

// Create an order
router.post('/create_order', auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		userId: auth.decode(request.headers.authorization).userId,
		userId: request.body.userId,
		productId: request.body.productId
	}

	UserController.createOrder(data).then((result) => {
		response.send(result)
	})
})


module.exports = router