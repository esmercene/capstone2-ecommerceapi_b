const User = require('../models/User')
const Product = require('../models/Product')
const bcrypt = require('bcrypt')
const auth = require('../auth')



// module to register new user
module.exports.register = (data)=> {
	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		email: data.email,
		password: encrypted_password,
		username: data.username
	})

	return new_user.save().then((created_user, error) => {
		if(error){
			return false
		}

		return { message: 'New User Successfully Registered'}
	})
}



// module to get all users
module.exports.getAllUsers = () => {
	return User.find({},{password: 0}).then((result) =>{
		return result
	})
}


// module to retrieve user details 
module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id, {password: 0}).then((result, error) => {
		return result
	})
}


// module to login user 
module.exports.login = (data) => {
	return User.findOne({username: data.username}).then((result) => {
		if (result == null){
			return {message: "User Doesn't Exist"}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)
		if(is_password_correct) {
			return { accessToken: auth.createAccessToken(result)}
		}

		return { message: "Invalid Password!!!"}
	})
}


// module to update a user(Admin only)
module.exports.updateUser = (user_Id, data) => {
	if(data.isAdmin){
		return User.findByIdAndUpdate(user_Id).then((result, error) => {
		if (error){
			return false
		    }

			result.isAdmin = data.isAdmin
			

			return result.save().then((updatedUser, error) => {
				if(error) {
					return false
	            }
			  return { message: `User with Id: ${user_Id} was Set as an Admin!`}
		    })
		})	
		}
		let message = Promise.resolve({message: "User must be ADMIN to update this"})
		return message.then((value) => {
			return value
        })		 	
}


// module to add an order
module.exports.createOrder = async (data) => {
		if(data.isAdmin){
			return Promise.resolve({ message: "Admin is not allowed to check out order!"})
		}
		let is_user_updated = await User.findById(data.userId).then((user) => {
		user.user_orders.push({
			totalAmount: data.totalAmount
		})

		user.user_orders[0].product.push({
			productId: data.productId,
			quantity: data.quantity
		})

		return user.save().then((updated_user, error) => {
			if(error){
				return false
			}

			return true
		})
	})

	let is_product_updated =  await Product.findById(data.productId).then((product) => {
		product.product_orders.push({
			orderId: data.orderId,
			userId: data.userId

		})

		return product.save().then((updated_product, error) => {
			if(error){
				return false
			}

			return true
		})
	})

	if(is_user_updated && is_product_updated){
		return { message: "User's Order was Successful!"}
     }
     return { message: "Order Unsuccessful!"}
}







