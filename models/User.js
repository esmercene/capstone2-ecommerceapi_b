const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Email is Required']
	},
	password: {
		type: String,
		required: [true, 'Password is Required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	username: {
		type: String,
	    required: [true, 'Username is Required']
		 },
	user_orders: [
	       {
	        product: [ 
	                     { 
	                 	  productId: {type: String},
				          quantity: {type: Number} 
				         }
				      ],
		    
		    totalAmount: {type: Number},    
		    
		    purchaseOn: {type: Date,default: new Date()}
		    }
	    ]
	    
})
module.exports = mongoose.model('User', user_schema)